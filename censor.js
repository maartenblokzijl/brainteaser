
function censor() {
    const map = new Object();
    return (a, b) => {
        if (!b) {
            return Object.keys(map)
                .reduce((acc, key) => acc.replace(key, map[key]), a);
        } else {
            map[a] = b;
        }
    }
}

const changeScene = censor();
changeScene('dogs', 'cats');
changeScene('quick', 'slow');
console.log(changeScene('The quick, brown fox jumps over the lazy dogs.')); // => should log 'The slow, brown fox jumps over the lazy cats.'